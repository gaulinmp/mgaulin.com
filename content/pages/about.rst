:title: Contact


.. image:: {filename}/static/Mac_Gaulin_Photo.jpg
   :width: 200 px
   :alt: Mac Gaulin


:Mobile:  `(512) 524-9736 <tel:+15125249736>`__
:Email:  `mpg@rice.edu <mailto:mpg@rice.edu>`__
:Website:  `mgaulin.com <http://mgaulin.com>`__
:Github:  `github.com/gaulinmp <https://github.com/gaulinmp>`__
:LinkedIn: `maclean-gaulin <https://linkedin.com/pub/maclean-gaulin/7/2b9/a7a>`_
:Address:  `Rice University <http://www.rice.edu>`__

            `Jesse H. Jones Graduate School of Business <http://business.rice.edu>`__

            359A McNair Hall

            6100 Main St.

            Houston, Texas 77005-1892
