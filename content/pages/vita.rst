:title: Vita

.. class:: resume

================================================================================
Maclean Gaulin
================================================================================


Contact
--------------------------------------------------------------------------------
..  .d8888b.                    888                      888
.. d88P  Y88b                   888                      888
.. 888    888                   888                      888
.. 888         .d88b.  88888b.  888888  8888b.   .d8888b 888888
.. 888        d88""88b 888 "88b 888        "88b d88P"    888
.. 888    888 888  888 888  888 888    .d888888 888      888
.. Y88b  d88P Y88..88P 888  888 Y88b.  888  888 Y88b.    Y88b.
..  "Y8888P"   "Y88P"  888  888  "Y888 "Y888888  "Y8888P  "Y888

.. container:: container pull-sm-left

    |RICE|_

    |JBS|_

    359a McNair Hall

    6100 Main St.

    Houston, Texas 77005


.. container:: container pull-sm-right

    :Phone: `(512) 524-9736 <tel:+15125249736>`__
    :Email: `gaulinmp@gmail.com <mailto:gaulinmp@gmail.com>`__
    :Web: `mgaulin.com <http://mgaulin.com>`__



|CLEAR|


Interests
--------------------------------------------------------------------------------
.. 8888888          888                                     888
..   888            888                                     888
..   888            888                                     888
..   888   88888b.  888888 .d88b.  888d888 .d88b.  .d8888b  888888 .d8888b
..   888   888 "88b 888   d8P  Y8b 888P"  d8P  Y8b 88K      888    88K
..   888   888  888 888   88888888 888    88888888 "Y8888b. 888    "Y8888b.
..   888   888  888 Y88b. Y8b.     888    Y8b.          X88 Y88b.       X88
.. 8888888 888  888  "Y888 "Y8888  888     "Y8888   88888P'  "Y888  88888P'

Public narrative disclosures, information acquisition, information usefulness, textual analysis.



Education
--------------------------------------------------------------------------------
.. 8888888888     888                            888    d8b
.. 888            888                            888    Y8P
.. 888            888                            888
.. 8888888    .d88888 888  888  .d8888b  8888b.  888888 888  .d88b.  88888b.
.. 888       d88" 888 888  888 d88P"        "88b 888    888 d88""88b 888 "88b
.. 888       888  888 888  888 888      .d888888 888    888 888  888 888  888
.. 888       Y88b 888 Y88b 888 Y88b.    888  888 Y88b.  888 Y88..88P 888  888
.. 8888888888 "Y88888  "Y88888  "Y8888P "Y888888  "Y888 888  "Y88P"  888  888

|RICE|_, |JBS|_, Houston, TX USA

    Ph.D. Candidate, Accounting (Expected 2017)

|BR|

|RHIT|_, Terre Haute, IN. (2003–2007)

    B.S. `Electrical Engineering <https://rose-hulman.edu/ece/>`_ (Summa cum-laude),
    Minor `Economics <https://rose-hulman.edu/econ/>`_


.. 8888888b.
.. 888   Y88b
.. 888    888
.. 888   d88P 8888b.  88888b.   .d88b.  888d888 .d8888b
.. 8888888P"     "88b 888 "88b d8P  Y8b 888P"   88K
.. 888       .d888888 888  888 88888888 888     "Y8888b.
.. 888       888  888 888 d88P Y8b.     888          X88
.. 888       "Y888888 88888P"   "Y8888  888      88888P'
..                    888
..                    888
..                    888

Working Papers
--------------------------------------------------------------------------------

* `Risk Fact or Fiction: The information content of risk factor disclosures </research/risk-fact-or-fiction-the-information-content-of-risk-factor-disclosures.html>`_, 2016 (job market paper)
* `Debt Contracting on Management <http://papers.ssrn.com/sol3/papers.cfm?abstract_id=2757508>`_ with Brian Akins and David De Angelis, 2016

    * Presented at the 2016 Lone Star Accounting Conference (2016), Academic Conference on Corporate Governance hosted by Drexel University (2016), Colorado Summer Accounting Research Conference (2016).

Work in process
--------------------------------------------------------------------------------

* Contracting on Performance and Risk-Taking: Creditors vs. Shareholders with Brian Akins, Jonathan Bitting, and David De Angelis, 2016




Teaching
--------------------------------------------------------------------------------
.. 88888888888                         888      d8b
..     888                             888      Y8P
..     888                             888
..     888   .d88b.   8888b.   .d8888b 88888b.  888 88888b.   .d88b.
..     888  d8P  Y8b     "88b d88P"    888 "88b 888 888 "88b d88P"88b
..     888  88888888 .d888888 888      888  888 888 888  888 888  888
..     888  Y8b.     888  888 Y88b.    888  888 888 888  888 Y88b 888
..     888   "Y8888  "Y888888  "Y8888P 888  888 888 888  888  "Y88888
..                                                                888
..                                                           Y8b d88P
..                                                            "Y88P"

**Rice University**

    Summer 2014 – Financial Accounting (BUSI 305)

      - Evaluations: 1.24 on scale from 7–1 (96.6%).



Conferences Attended
--------------------------------------------------------------------------------
..  .d8888b.                     .d888
.. d88P  Y88b                   d88P"
.. 888    888                   888
.. 888         .d88b.  88888b.  888888 .d8888b
.. 888        d88""88b 888 "88b 888    88K
.. 888    888 888  888 888  888 888    "Y8888b.
.. Y88b  d88P Y88..88P 888  888 888         X88
..  "Y8888P"   "Y88P"  888  888 888     88888P'

:2015:  Financial Accounting and Reporting Section Midyear Meeting. (Discussant of *The Effect of Institutional Ownership on Firm Transparency and Information Production* by Audra Boone and Joshua White; `SSRN <http://ssrn.com/abstract=2528891>`__)

        - Loan Star Accounting Conference

:2014:  Olin Business School conference. (Co-discussant of *Textual Analysis and International Financial Reporting: Large Sample Evidence* by Mark Lang and Lorien Stice-Lawrence; `SSRN <http://ssrn.com/abstract=2407572>`__)

        - AAA/Deloitte/J. Michael Cook Doctoral Consortium

        - Financial Accounting and Reporting Section Midyear Meeting

        - International Accounting section of the AAA

        - Loan Star Accounting Conference


Work Experience
--------------------------------------------------------------------------------
.. 888       888                  888           8888888888
.. 888   o   888                  888           888
.. 888  d8b  888                  888           888
.. 888 d888b 888  .d88b.  888d888 888  888      8888888    888  888 88888b.
.. 888d88888b888 d88""88b 888P"   888 .88P      888        `Y8bd8P' 888 "88b
.. 88888P Y88888 888  888 888     888888K       888          X88K   888  888
.. 8888P   Y8888 Y88..88P 888     888 "88b      888        .d8""8b. 888 d88P
.. 888P     Y888  "Y88P"  888     888  888      8888888888 888  888 88888P"
..                                                                  888
..                                                                  888
..                                                                  888

Graduate Researcher - |RICE|_ - 2011–Present

    Supervised by Karen Nelson, Brian Akins

|BR|

Application Engineer -
`Graftek Imaging <https://graftek.biz/>`_ -
2010–2011

    *Overview*: Developed products for vision inspection and control systems, both on a team and independently.

|BR|

Application Engineer/Field Sales Engineer -
`National Instruments <http://ni.com>`_ -
2007–2009

    *Overview*: Worked in application engineering on technical support and proof of concept project development.
    Responsible for field sales in Northern Alberta, Saskatchewan, and Manitoba.


|BR|
|BR|

.. 8888888b.           .d888
.. 888   Y88b         d88P"
.. 888    888         888
.. 888   d88P .d88b.  888888 .d88b.  888d888 .d88b.  88888b.   .d8888b .d88b.  .d8888b
.. 8888888P" d8P  Y8b 888   d8P  Y8b 888P"  d8P  Y8b 888 "88b d88P"   d8P  Y8b 88K
.. 888 T88b  88888888 888   88888888 888    88888888 888  888 888     88888888 "Y8888b.
.. 888  T88b Y8b.     888   Y8b.     888    Y8b.     888  888 Y88b.   Y8b.          X88
.. 888   T88b "Y8888  888    "Y8888  888     "Y8888  888  888  "Y8888P "Y8888   88888P'

References
--------------------------------------------------------------------------------
**K. Ramesh** - Herbert S. Autrey Professor of Accounting

    *Phone:* 713-348-5380 |BR|
    *Email:* `rameshk@rice.edu <mailto:rameshk@rice.edu>`_ |BR|
    *Address:* |BR| Rice University |BR|
    236 McNair Hall |BR|
    6100 Main St  |BR|
    Houston, TX 77005

|BR|

**Brian Rountree** - Associate Professor of Accounting

    *Phone:* 713-348-5328 |BR|
    *Email:* `rountree@rice.edu <mailto:rountree@rice.edu>`_  |BR|
    *Address:* |BR| Rice University  |BR|
    340 McNair Hall |BR|
    6100 Main St  |BR|
    Houston, TX 77005

|BR|


**Shiva  Sivaramakrishnan** - Henry Gardiner Symonds Professor of Accounting

    *Phone:* 713-348-4653 |BR|
    *Email:* `ks30@rice.edu <mailto:ks30@rice.edu>`_  |BR|
    *Address:* |BR| Rice University  |BR|
    336 McNair Hall |BR|
    6100 Main St  |BR|
    Houston, TX 77005



|BR|
|BR|

Updated: 2016-11-11


.. 888      8888888 888b    888 888    d8P   .d8888b.
.. 888        888   8888b   888 888   d8P   d88P  Y88b
.. 888        888   88888b  888 888  d8P    Y88b.
.. 888        888   888Y88b 888 888d88K      "Y888b.
.. 888        888   888 Y88b888 8888888b        "Y88b.
.. 888        888   888  Y88888 888  Y88b         "888
.. 888        888   888   Y8888 888   Y88b  Y88b  d88P
.. 88888888 8888888 888    Y888 888    Y88b  "Y8888P"

.. |JBS| replace:: Jesse H. Jones Graduate School of Business

.. _JBS: http://business.rice.edu

.. |RICE| replace:: Rice University

.. _RICE: http://www.rice.edu

.. |LinkedIn| replace:: LinkedIn

.. _LinkedIn: https://linkedin.com/pub/maclean-gaulin/7/2b9/a7a

.. |Github| replace:: github.com/gaulinmp

.. _Github: https://github.com/gaulinmp

.. |RHIT| replace:: Rose-Hulman Institute of Technology

.. _RHIT: http://rose-hulman.edu/



.. |CLEAR| raw:: html

  <div class="clearfix">&nbsp;</div>


.. |BR| raw:: html

  <br />

.. |nbsp| unicode:: 0xA0
   :trim:
