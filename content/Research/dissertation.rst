:title: Risk Fact or Fiction: The information content of risk factor disclosures
:authors: Maclean Gaulin (job market paper)
:tags: disclosure,risk factors,information overload
:date: 2016-10-31
:hasabstract: True


I investigate whether managers disclose timely and informative risk factors by directly testing whether risk factor disclosures forecast adverse outcomes.
I develop a set of measures based on the time series evolution of individual risk factors that reflect a manager's decision to add new, retain existing, and remove obsolete risk factors across annual reports.
Consistent with informative disclosure practices, I find managers are more likely to identify new risk factors and less likely to remove existing risk factors in advance of adverse outcomes such as negative earnings or business litigation events.
The predictive ability of these measures generally outperforms other risk disclosure proxies used in extant literature, and they are robust to the inclusion of controls for ex-ante risk and firm performance.
By focusing on time series variation, I further study how risk factor disclosures respond to direct demand from two sources: shareholders (via private enforcement actions in the form of securities litigation) and regulators (via public enforcement actions in the form of SEC comment letters).
Consistent with the litigation shield hypothesis, I find that private enforcement actions result in increased identification of new risk factors and removal of existing risk factors, and that this effect persists for multiple years.
In contrast, I find that public enforcement results in no net increase in risk factors identified, but does result in more definitive disclosures as measured by more specific language and increased use of numbers.
