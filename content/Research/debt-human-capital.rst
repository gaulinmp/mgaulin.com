:title: Debt Contracting on Management
:authors: Brian Akins, David De Angelis, Maclean Gaulin
:paperstatus: Working Paper
:tags: debt,contracting,human capital,covenants
:date: 2016-10-17
:ssrn: http://papers.ssrn.com/sol3/papers.cfm?abstract_id=2757508
:hasabstract: True

This paper documents and studies the use of change of management restrictions (CMRs) in private loan contracts.
These clauses give lenders explicit control over retention and/or selection decisions.
Our analysis suggests CMRs are binding: the likelihood of CEO turnover more than halved during CMR terms.
We find lenders include CMRs to mitigate human capital risk, particularly when this risk is exacerbated by contracting frictions or by creditor-shareholder conflicts of interest.
Overall, our results imply that lenders can influence managerial turnover outside of default states, and human capital risk can impact debt contract design in ways not anticipated in the literature.
